import _ from 'lodash';


export const findObjectInArray = (arrayList, arrKey, obj, objKey) =>{
    let indexInArray = _.findIndex(arrayList, function(o) { 
        return o[arrKey] === obj[objKey]; 
    });
     return indexInArray;
}
