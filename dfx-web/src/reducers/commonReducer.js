import * as types from '../Utils/constants';

const initialState = {
    enableLoader:false,
};

export default (state = initialState, action) => {
    switch (action.type) {

        case types.FETCH_REQUESTED_LOADER_ENABLE:
        return {
            ...state,
            enableLoader: true
        }
        case types.FETCH_REQUESTED_LOADER_DISABLE:
        return {
            ...state,
            enableLoader: false
        }
        default:
        return state
    }
}
