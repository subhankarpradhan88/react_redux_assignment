import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import commonReducer from './commonReducer';
import adminReducer from './adminReducer';
import {reducer as toastrReducer} from 'react-redux-toastr';

export default combineReducers({
  toastr: toastrReducer,
  router: routerReducer,
  adminReducer,
  commonReducer
});
