import * as types from '../Utils/constants';

const initialState = {
    addCodeList:[],
    fetchCodeList:[],
    currentCodeList: [],
    delCodeObj : {}
};

export default (currentState = initialState, action) => {
    switch(action.type) {
        case types.ADD_DFM_ISSUE_CODES:
            return {
                ...currentState,
                addCodeList: action.payload && [action.payload]
            }
        case types.GET_DFM_ISSUE_CODES:
            return {
                ...currentState,
                fetchCodeList: action.payload.sort(function (a, b) {
                    return a.updatedDate - b.updatedDate;
                })       
            }
        case types.DELETE_ISSUE_CODE:
            return {
                ...currentState,
                currentCodeList: action.updatedListPayload
            }
        case types.DELETE_PROMPT_HANDLER:
            return {
                ...currentState,
                delCodeObj: {
                    selIndex: action.codeIndex,
                    selCode: action.payloadCode
                }
            }
        default:
            return currentState;
    }
}
