import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';
import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import store, { history } from './store'
import App from './components/app'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import ReduxToastr from 'react-redux-toastr';
import { LastLocationProvider } from 'react-router-last-location';
render(
  <Provider store={store}>  
    <ConnectedRouter history={history}>
      <LastLocationProvider>
      <div>
      <ReduxToastr
      timeOut={4000}
      newestOnTop={false}
      preventDuplicates
      position="top-right"
      transitionIn="fadeIn"
      transitionOut="fadeOut"
      progressBar
      closeOnToastrClick/>
        <App />
      </div>
      </LastLocationProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

if (module.hot) {
  module.hot.accept();
}
