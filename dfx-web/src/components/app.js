import React,{Fragment} from 'react';
import { Route, Redirect, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Dashboard from './Dashboard/dashboard'
import Main from './Header/main.js';
import Header from './Header/header';
import GeneralError from './CommonComponents/generalError';
import Loader from './CommonComponents/loader';
import SessionModal from './CommonComponents/SessionModal';
import admin from './Admin/admin';

class App extends React.Component {
	constructor( props ) {
		super( props );
	}

	state = {
		logOutModal: false
	}
	inactivityTime = () => {
		let time;
		// Tracking different event listeners
		window.addEventListener('load', this.resetTimer, true);
		const events = ['click','mousedown','mousemove','keypress','scroll','touchstart'];
		events.forEach((name)=> {
			document.addEventListener(name, this.resetTimer, true);
		})
		// User will be logged out after 1 hour
		this.logout = (e) => {
			this.setState({
				logOutModal: true
			})
		}
		// Reseting the timer on any activity
		this.resetTimer = (e) => {
			clearTimeout(time);
			time = setTimeout(this.logout, 3600000);
		}
	}
	render() {
		this.inactivityTime(); 
		let { enableLoader } = this.props;
		return (
				<Fragment>
				{this.state.logOutModal ? <SessionModal/> : ''}
				<Header loggedInUser="Lorem Ipsum"/>
		        <main className="main-page">
					<Main loggedInUser="Lorem Ipsum"/>
						<Switch>
							<Route exact path="/" component={Dashboard} />
							<Route exact path="/admin" component={admin} />
							<Route render={()=><GeneralError />} />
						</Switch>
					<Loader enableLoader={enableLoader} />
		        </main>
		      </Fragment>
		);
	}
}

const mapStateToProps = state => {
	return{
		enableLoader : state.commonReducer.enableLoader,
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps, null, {pure:false})(App);