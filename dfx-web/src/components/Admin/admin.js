import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import Switch from '@material-ui/core/Switch';
import { withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import InfoSharpIcon from '@material-ui/icons/InfoSharp';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import WarningRoundedIcon from '@material-ui/icons/WarningRounded';
import Footer from '../CommonComponents/footer';
import * as moment from 'moment-timezone';
import _ from 'lodash';
import { Prompt } from 'react-router';
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";

import { addDFMIssueCodes, getDFMIssueCodes, delIssueCode, saveDFMIssueCodes, delPromptHandler } from '../../actions/adminAction';
import './admin.css';

class Admin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            issueCode: '',
            issueDesc: '',
            currentList: [],
            name: true,
            codeVal: true,
            descVal: true,
            statusChangedCode: [],
            statusEdited: [],
            notification: false,
            sortingImg: true,
            sortingName: '',
            count: 0,
            issueCodeString: "",
            sort: {
                column: null,
                direction: 'desc'
            },
            setOpen: false
        };
    }

    componentDidMount() {
        let { addCodeList } = this.props;
        let emptyObj = {};
        this.props.getDFMIssueCodes();
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
        this.setState({
            statusChangedCode: [],
            statusEdited: []
        });
    }
    componentDidUpdate(prevProps) {
        if(this.props.fetchCodeList !== prevProps.fetchCodeList) {
            this.setState({
                currentList: this.props.fetchCodeList
            });
        }else if(prevProps.currentCodeList !== this.props.currentCodeList) {
            this.setState({
                currentList: this.props.currentCodeList
            });
        }
    }
    handleChange = (name, index, code) => (event) => {
        let arrayA = [], arrayB = [];
        let currentList = [...this.state.currentList];
        for(let i = 0; i < this.state.statusEdited.length; i++) {
            if(this.state.statusEdited[i].issueCode === code) {
                this.state.statusEdited.splice(i,1);
                i--;
            }
        }
        for(let i = 0; i < currentList.length; i++) {
            if(currentList[i].issueCode === code) {
                if(currentList[i].status == true) {
                    currentList[i].status = false;
                    this.state.statusChangedCode.push(currentList[i].issueCode);
                    this.state.statusEdited.push(currentList[i]);  
                }else if(currentList[i].status == false) {
                    currentList[i].status = true
                    this.state.statusChangedCode.push(currentList[i].issueCode);
                    this.state.statusEdited.push(currentList[i]);  
                }
                arrayA = this.state.statusChangedCode.filter((item,index) => this.state.statusChangedCode.indexOf(item) !== index);
                arrayB = arrayA.filter(key => key !== this.state.statusChangedCode.map(item => item));
                for(let i = 0; i < this.state.statusChangedCode.length; i++) {
                    for(let j = 0; j < arrayB.length; j++) {
                        if((arrayB[j] === this.state.statusChangedCode[i])) {
                            this.state.statusChangedCode.splice(i,1);
                            i--;
                        }
                    }
                }
            }   
        }
        this.setState({ 
            currentList: currentList,
            notification: true
        });
    }
    addIssueCode = (e) => {
        e.preventDefault();
        let {fetchCodeList} = this.props;
        if(this.state.issueCode) {
            this.setState({
                codeVal: true,
                issueCodeString: ''
            })
        }else {
            this.setState({
                codeVal: false,
                issueCodeString: "Please Provide Issue Code"
            })
        }
        if(this.state.issueDesc) {
            this.setState({
                descVal: true
            })
        }else {
            this.setState({
                descVal: false
            })
        }

        let PSTTime = new Date().getTime();
        let codeObj = {};
        codeObj.issueCodeId = 0;
        codeObj.issueCode = this.state.issueCode;
        codeObj.description = this.state.issueDesc;
        codeObj.createDate = PSTTime;
        codeObj.updatedDate = PSTTime;
        codeObj.lastUpdatedBy = fetchCodeList[fetchCodeList.length - 1].lastUpdatedBy;
        codeObj.status = true;
        try{
            if(this.state.issueCode.length === 17 && this.state.issueDesc && !((this.state.currentList && this.state.currentList.length > 0) && this.state.currentList.some(o => o.issueCode === this.state.issueCode))) {
                this.props.addDFMIssueCodes(codeObj);
                this.setState({
                    codeVal: true,
                    issueCodeString: '',
                    issueCode: '',
                    issueDesc: ''
                })
            }else if(this.state.issueCode.length > 0 && (this.state.issueCode.length < 17 || this.state.issueCode.length > 17)) {
                this.setState({
                    codeVal: false,
                    issueCodeString: "Please Provide 17 characters",
                    issueCode: ''
                })
            }else if(!this.state.issueCode) {
                this.setState({
                    codeVal: false,
                    issueCodeString: "Please Provide Issue Code"
                })
            }
        }catch(err) {
            console.log(err);
        }finally {
            if((this.state.currentList && this.state.currentList.length > 0) && this.state.currentList.some(o => o.issueCode === this.state.issueCode)) {
                this.setState({
                    codeVal: false,
                    issueCodeString: 'Issue Code Already Exists',
                    issueCode: ''
                });
            }
        }
    }
    issueCodeVal = (e) => {
        let val = e.target.value; 
        var eleVal = val && val.trim();
        //Base Condition
        if(val === "_" || val === "+" || val === " " || val.length > 17) {
            return;
        }else {
            eleVal = val && val.replace(/[^\w\s]/gi, '').replace(/ /gi, '');
        }
        // Add dash after every 4 characters
        let finalVal = eleVal && eleVal.match(/.{1,4}/g).join('-').toUpperCase();
        finalVal = finalVal.trim();
        this.setState({
            issueCode: finalVal
        })
        if(this.state.issueCode) {
            this.setState({
                codeVal: true
            })
        }
        let { addCodeList } = this.props;
        let emptyObj = {};
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
    }
    issueCodeDesc = (e) => {
        let valDesc = e.target.value;
        let { addCodeList } = this.props;
        let emptyObj = {};
        // Base Condition
        if(valDesc == " " || valDesc.length > 100) {
            return;
        }
        this.setState({
            issueDesc: valDesc
        })
        if(this.state.issueDesc.trim()) {
            this.setState({
                descVal: true
            })
        }
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
    }
    handleClickOpen = (index, issCode) => (event) => {
        let { addCodeList } = this.props;
        let emptyObj = {};
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
        this.setState({
            setOpen: true
        });
        this.props.delPromptHandler(index, issCode);
    };
    handleClose = () => {
        this.setState({
            setOpen: false
        })
    };
    deleteHandler = () => {
        let { delCodeObj } = this.props;
        let {selIndex, selCode} = delCodeObj;
        let changeCodeList = [], editedList = [];
        let currentList = [...this.state.currentList];
        changeCodeList = this.state.statusChangedCode.filter(key => key !== selCode);
        editedList = this.state.statusEdited.filter(obj => obj.issueCode !== selCode);
        currentList.splice(selIndex, 1);
        this.setState({ 
            statusEdited: editedList,
            statusChangedCode: changeCodeList
        });
        try{
            this.props.delIssueCode(currentList);
        }catch(err) {
            console.log(err);
        }finally {
            this.setState({
                setOpen: false
            });
        }
    }
    sortDisplay = (sortbyname) => {
        let {addCodeList} = this.props;
        let emptyObj = {};
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
        this.setState({
            sortingImg: true,
            count: this.state.count + 1,
            sortingName: sortbyname
        })
        if(this.state.count % 2 === 0 || this.state.count == 0){
            this.setState({
                sortingImg: false
            })
        }
        const direction = this.state.sort.column ? (this.state.sort.direction === 'asc' ? 'desc' : 'asc') : 'desc';
        const sortedData = this.state.currentList.sort((a, b) => {
            if (sortbyname === 'issueCode') {
                const nameA = a.issueCode.toUpperCase(); // ignore upper and lowercase
                const nameB = b.issueCode.toUpperCase(); // ignore upper and lowercase
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                // names must be equal
                return 0;
            }
            else if(sortbyname === 'created') {
                const nameA = a.createDate; 
                const nameB = b.createDate; 
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            }else if(sortbyname === 'updatedDate') {
                const nameA = a.updatedDate; 
                const nameB = b.updatedDate; 
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                if (nameA == nameB) {
                    return 0;
                }
            }else if(sortbyname === 'lastUpdatedBy') {
                const nameA = a.lastUpdatedBy.toUpperCase(); 
                const nameB = b.lastUpdatedBy.toUpperCase(); 
                if (nameA < nameB) {
                    return -1;
                }
                if (nameA > nameB) {
                    return 1;
                }
                return 0;
            }
        });  
        if (direction === 'desc') {
            sortedData.reverse();
        }
        this.setState({
            currentList: sortedData,
            sort: {
                column: sortbyname,
                direction
            }
        });
    }
    saveIssueCode = () => {
        let editedIssueCodes = [...this.state.statusEdited];
        for(let obj of editedIssueCodes) {
            delete obj.createdBy;
            delete obj.createDate;
            delete obj.updatedDate;
        }
        let { addCodeList, history } = this.props;
        let emptyObj = {};
        if(addCodeList.length > 0) {
            this.props.addDFMIssueCodes(emptyObj);
        }
        try{
            if(editedIssueCodes && editedIssueCodes.length > 0) {
                this.props.saveDFMIssueCodes(editedIssueCodes, history);
            }
        }catch(err) {
            console.log(err);
        }finally {
            this.setState({
                notification: false,
                statusChangedCode: [],
                statusEdited: []
            });
        }
    }

    render() {
            const {fetchCodeList, addCodeList, currentCodeList, templateDate} = this.props;
            const GreenSwitch = withStyles({
                input: {
                    opacity: 0,
                    width: 0,
                    height: 0,
                },
                switchBase: {
                    '&$checked': {
                      color: green[500],
                      cursor: 'pointer'
                    },
                    '&$checked + $track': {
                      backgroundColor: green[500],
                    },
                  },
                  checked: {},
                  track: {},
                })(Switch);
                
            // Check for duplicates and falsey value
            if((fetchCodeList && fetchCodeList.length > 0) && (currentCodeList.length === 0)) {
                fetchCodeList.forEach(obj => {
                    if (!this.state.currentList.some(o => o.issueCode === obj.issueCode)) {
                        if(obj.issueCode && obj.description) {
                            this.state.currentList.push({ ...obj }); // Fetches the issue codes and pushes in the state
                        }
                    }
                });
            }
            
            if(addCodeList && addCodeList.length > 0) {
                addCodeList.forEach(obj => {
                    if (!this.state.currentList.some(o => o.issueCode === obj.issueCode)) {
                        if(obj.issueCode && obj.description) {
                            this.state.currentList.unshift({ ...obj }); // Adds the input object one after another in the main array
                            this.state.statusEdited.push({ ...obj });
                        }
                    }  
                });
            }
            const uniqueList = [...new Set(this.state.currentList.map(obj => obj))];
            const currentIssueCodes = (uniqueList && uniqueList.length > 0) ? uniqueList.map((item, idx) => {
            return (
                <tr className="transition-enabled" key={item.issueCode}>
                    <td className="width30 center-align padding-top-small">
                        {idx + 1}
                    </td>
                    <td className="width145 paddingCol">
                        {item.issueCode}
                    </td>
                    <td className="width370 textWrap paddingCol">
                        {item.description}
                    </td>
                    <td className="width80 paddingCol">
                        {moment.utc(item.createDate).tz('America/Los_Angeles').format('MM/DD/YYYY')}
                    </td>
                    <td className="width90 paddingCol">
                        {moment.utc(item.updatedDate).tz('America/Los_Angeles').format('MM/DD/YYYY')}
                    </td>
                    <td className="width110 textWrap paddingCol">
                        {item.lastUpdatedBy}
                    </td>
                    <td className="padding-top-small switchWrapper">
                        <label>
                            <GreenSwitch
                                id="on-off-switch"
                                checked={item.status}
                                name= {"checked" + idx}
                                onChange={this.handleChange("checked" + idx, idx, item.issueCode)}
                                inputProps={{ 'aria-label': 'Switch' }}
                                value={item.status}
                            />
                        </label>
                        {(this.state.notification && this.state.statusChangedCode.some(code => code == item.issueCode)) && <div className="statusTooltip">
                            <InfoSharpIcon className="infoIconStyle"/>
                            <span className="statusTooltiptext">Status Changed</span>
                        </div>}
                        {(!item.issueCodeId && this.state.statusEdited.some(obj => obj.issueCode == item.issueCode)) && <div className="delTooltip">
                            <DeleteForeverIcon className="delIconStyle" onClick={this.handleClickOpen(idx, item.issueCode)} />
                            <span className="delTooltiptext">Delete Row</span>
                        </div>}
                    </td>
                </tr>
            )
        }): 
        <tr>
            <td colSpan="3"></td>
            <td align="center">No Issue Codes to Display</td>
            <td colSpan="3"></td>
        </tr>;
        return (
                <div className="mainWrapper">
                    <Dialog
                    open={this.state.setOpen}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description">
                        <DialogContent>
                        <DialogContentText id="alert-dialog-description" className="flexContainer dialogContent">
                            <span className="warningStyle">
                                <WarningRoundedIcon className="warningIconStyle"/>
                            </span>
                            <span className="delText">You are about to delete this Issue Code</span>
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <button className="adminPopupCancel" onClick={this.handleClose.bind(this)}>Cancel</button>
                            <button className="adminPopupOk" onClick={this.deleteHandler.bind(this)}>Ok</button>
                        </DialogActions>
                    </Dialog>
                    
                    <Prompt
                        when={(this.state.statusChangedCode.length > 0) || (this.state.statusEdited.some(obj => obj.issueCodeId == 0))}
                        message='You have unsaved changes, are you sure you want to leave this page?'
                    />
                    <span className="flexContainer">
                        <p className="font-size-large text-color-grey-dark screenTitleStyle">Issue Code Management</p>
                        <div className="updatedByTextWrapper">
                            <p className="alignRight"><strong>Last Updated by :</strong> <img className="img-link text-align-middle" src="/images/icon-user.png" atl="contact" title="contact" width={24} />&nbsp;&nbsp;{fetchCodeList.length > 0 && fetchCodeList[fetchCodeList.length - 1].lastUpdatedBy} | <span> {fetchCodeList.length > 0 && moment.utc(fetchCodeList[fetchCodeList.length - 1].updatedDate).tz('America/Los_Angeles').format('MM/DD/YYYY, h:mm:ss A')} PST</span></p>
                        </div>
                    </span>
                    <form className="form-inline" ref="issueCodeForm" onSubmit={this.addIssueCode}>
                        <div className='addCodeWrapper'>
                            <div className="flexContainer">
                                <div className="issueCodeTextStyle"><p className="text-input-label"><span className="text-required">*</span> Issue Code <span className="issueCodeValidate"> (17 char. required)</span></p></div>
                                <div className="issueDescTextStyle"><p className="text-input-label"><span className="text-required">*</span> Issue Description  <span className="issueDescValidate"> (Max. 100 char.)</span></p></div>
                            </div>
                            <div className="flexContainer">
                                <div className="issueCodeTextWrapper">
                                    <input type="text" name="issueCode" autoComplete="off" value={this.state.issueCode} onChange={this.issueCodeVal}/>
                                    {!this.state.codeVal && <span className="issueCodeString">{this.state.issueCodeString}</span>}
                                </div>
                                <div className="descTextWrapper">
                                    <input type="text" name="issueDescription" autoComplete="off" className="marginLeft-10" value={this.state.issueDesc} onChange={this.issueCodeDesc} maxLength="100" />
                                    {!this.state.descVal && <span className="issueDescString">Please Provide Issue Description</span>}
                                </div>
                                <div className="addBtnStyle"><button type="submit" className="button-submit-green">Add</button></div>   
                            </div>
                        </div>
                    </form>
                    <div className="editCodeGrid padding-around">
                        <div className="long-column-list-container h-scroll v-scroll-515 no-padding-top">
                            <table id="issueCodesList" className="tableAdmin list font-size-small table-cursor">
                                <thead>
                                <tr>
                                    <th className="center-align sticky-top width30">No.</th>
                                    <th className="sticky-top width145" onClick={this.sortDisplay.bind(this,'issueCode')}>Issue Code  {this.state.sortingName == 'issueCode' && this.state.sortingImg === true ? '▲' : '▼'}</th>
                                    <th className="sticky-top width370">Issue Description</th>
                                    <th className="sticky-top width80" onClick={this.sortDisplay.bind(this,'created')}>Created  {this.state.sortingName == 'created' && this.state.sortingImg === true ? '▲' : '▼'}</th>
                                    <th className="sticky-top width90" onClick={this.sortDisplay.bind(this,'updatedDate')}>Last Updated  {this.state.sortingName == 'updatedDate' && this.state.sortingImg === true ? '▲' : '▼'}</th>
                                    <th className="sticky-top width110" onClick={this.sortDisplay.bind(this,'lastUpdatedBy')}>Updated by  {this.state.sortingName == 'lastUpdatedBy' && this.state.sortingImg === true ? '▲' : '▼'}</th>
                                    <th className="sticky-top width110">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {currentIssueCodes}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="legendsContainer">
                        <div className="legendContentWrapper">
                            <div className="legendActiveIcon"></div>
                            <div className="legendActiveText">Active</div>
                            <div className="legendInActiveIcon"></div>
                            <div className="legendInActiveText">In Active</div>
                            <div className="legendErrorIconWrapper">
                                <InfoSharpIcon className="infoLegendStyle"/>
                            </div>
                            <div className="legendEditText">Edited</div>
                            <div className="legendDelWrapper">
                                <DeleteForeverIcon className="deleteLegendStyle"/>
                            </div>
                            <div className="legendDelTextStyle">Delete</div>
                        </div>
                        <div className="actionBtnWrapper alignRight">
                            <Link className="button-cancel margin-right" to="/">Cancel </Link>
                            <button className="primary" onClick={this.saveIssueCode.bind(this, 'save')}>Save</button>
                        </div>
                    </div>
                    <Footer/>
                </div>
        )
    }
}

const mapStateToProps = state => {
	return{
        fetchCodeList: state.adminReducer.fetchCodeList || [],
        addCodeList: state.adminReducer.addCodeList || [],
        currentCodeList: state.adminReducer.currentCodeList || [],
        delCodeObj: state.adminReducer.delCodeObj || {}
	}
}
// Dispatch Action
const mapDispatchToProps = dispatch => bindActionCreators({
    addDFMIssueCodes,
    getDFMIssueCodes,
    delIssueCode,
    saveDFMIssueCodes,
    delPromptHandler
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Admin);