import React from 'react';
import Footer from '../CommonComponents/footer';
 
class Dashboard extends React.Component {
	constructor( props ) {
		super( props );
		this.state = {}
	}
	redirectRoute(name){
		this.props.history.push(name);
	}
	render() {
		return (
				<div className="content" data-test="dashboardComponent">
					<div className="padding-top padding-left">
						<p className="font-size-large text-color-grey-dark">Dashboard</p>
						<p className="font-size-small text-color-grey-dark">User Role : ADMIN</p>
					</div>
					<div className="tiles padding-left">
						<div className="tile" onClick={() => this.redirectRoute('/admin')}>
						<div className="title">
							<div className="center-align"><span className="counter-title">Admin</span></div>
						</div>
						<div className="value">
							<div className="center-align"><span className="text-color-navy">Lorem Ipsum</span></div>
						</div>
						</div>
					</div>
					<div className="padding-top padding-left">&nbsp;&nbsp;<span className="text-color-red-dark"><strong>Note :</strong> All dates and times are in PST</span></div>
					<Footer />
				</div>  
			)
		}
	}

export default Dashboard;
