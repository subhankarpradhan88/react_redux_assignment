import React ,{Component, Fragment} from 'react'; 
import {NavLink, Link} from 'react-router-dom';
export default class Main extends Component {
    render(props){
        return(
            <Fragment>
		        <div className="padding-top nav-left-external">
		            <nav className="vertical full-height-page">
						<ul className="leftNavWrapper">
		                <li><Link className={(document.location.pathname === "/" ? "left-nav-focused" : "")} to="/">Dashboard</Link></li>
						<li><NavLink activeClassName="left-nav-focused" to="/admin">Admin</NavLink></li>
		              </ul>
		            </nav>
				</div>
			</Fragment>
        )
    }
}