	import React, {Component} from 'react';  

	const Header = (props) => {
		let { loggedInUser } = props;
		if(Object.keys(loggedInUser).length !== 0 && loggedInUser.constructor !== Object){
			localStorage.setItem('loggedInUserDetails',JSON.stringify(loggedInUser));}
		return(
			<header>
					<div>
						<h1 className="app"><a href="/"><span className="brand-link">React & Redux - Assignment</span></a></h1>
					</div>
					<div className="media">
						<div className="media-left">
							<span className="avatar compact" />
						</div>
						<div className="media-body">
							<span className="user-name">Hi, {props.loggedInUser} </span>
						</div>
						<div>
							<a href="/logout"><span className="logout">Logout</span></a>
						</div>
					</div>
			</header>
		)
	}
	export default Header;

