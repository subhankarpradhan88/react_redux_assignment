import React, { Component } from "react"; 

class SessionModal extends Component {
    constructor(props) {
        super(props);
        this.logout = this.logout.bind(this);
        this.state = {
          showModal: true
        }
    }
    componentDidMount() {
      this.logout();
    }
    logout = (e) => {
      setTimeout(() => {
        window.location.replace("/generalError");
        this.setState({
          showModal: false
        });
      },5000);
    }
  render() {
    return (
      <div className={this.state.showModal === true ? "show attachment-popup-dialog" : "hidden"}>
        <div className="overlay-container">
          <div className="overlay-backdrop" />
          <div className="sessionDialog">
            <h1 className="center-align"> Session Timeout </h1>
            <div className="center-align">
              <img class="text-align-middle" src="/images/watch.png" width="45" alt="" />
            </div>
            <div>
                <p></p>
                <p></p>
                <p></p>
            </div>
            <div className="center-align"><h4>Session has expired, you will be logged out.</h4></div>
          </div>
        </div>
      </div>
    );
  }
}

export default SessionModal;