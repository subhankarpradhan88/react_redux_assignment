import React from 'react';

const Footer = () => {
    return(
        <div className="padding-top-largest padding-left">
            <p className="copyrights">© 2020 IBM. (React & Redux - Assignment)</p>
        </div>
    )
}
export default Footer;