import React from 'react';

const Loader = (props) => {
    return(
        <div className={props.enableLoader ? "show loading-overlay" : "hidden"}>
            <div className="overlay-container">
            <div className="overlay-backdrop" />
            <div className="dialog1">
                <div>
                <img src="/images/loading.svg" alt="loading.." height={45} />
                </div>
            </div>
            </div>
        </div>
    )
}

export default Loader;