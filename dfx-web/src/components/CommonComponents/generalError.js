import React from 'react';
import {Link} from 'react-router-dom';

import Footer from '../CommonComponents/footer';

const GeneralError = _ => (
	<div className="center-align content">
		<div className="padding-top-largest">
			<p className="font-size-large text-color-red-dark">
			<img className="img-align-middle" src="stencil/images/alert-red.png" width="20" />  Looks like something went wrong!</p>
			<p className="font-size-large text-color-grey-dark padding-top-larger">Sorry for the inconvenience, we are working on it.</p>
		</div>
		<div className="padding-top">
			<Link to="/"><button className="primary">Go back to DashBoard</button></Link>
		</div>
		<Footer/>
  </div>
);
export default GeneralError;