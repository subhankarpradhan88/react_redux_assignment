import React from 'react';
import axios from 'axios';
import * as types from '../Utils/constants';
import {toastr} from 'react-redux-toastr';

export const addDFMIssueCodes = (issueCodeItem) => {
    let addIssueCodeAction = {
        type: types.ADD_DFM_ISSUE_CODES,
        payload: issueCodeItem
    }
    return addIssueCodeAction;
}
export const delIssueCode = (listState, idx) => {
    let updatedState = {
        type: types.DELETE_ISSUE_CODE,
        updatedListPayload: listState,
        delCodeIdx: idx
    }
    return updatedState;
}
export const delPromptHandler = (idx, code) => {
    let deleteState = {
        type: types.DELETE_PROMPT_HANDLER,
        codeIndex: idx,
        payloadCode: code
    }
    return deleteState;
}
export const getDFMIssueCodes = () => {
    let issueCodeAPI = `/testJson/issueCode.json`;
    return dispatch => {
        dispatch({ type: types.FETCH_REQUESTED_LOADER_ENABLE });
        axios.get(issueCodeAPI)
        .then(function(response) {
            dispatch({
                type: types.GET_DFM_ISSUE_CODES,
                payload: response.data
            });
            dispatch({ type: types.FETCH_REQUESTED_LOADER_DISABLE });
        })
        .catch(function(error) {
            dispatch({ type: types.FETCH_REQUESTED_LOADER_DISABLE });
        })  
    }
}

export const saveDFMIssueCodes = (dfmIssueCodes) => {
    toastr.success("Issue Codes has been saved");
    console.log("Saved Issue Codes : ", dfmIssueCodes);
}