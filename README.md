# REACT_REDUX_ASSIGNMENT

The application is a module for ADMIN’s which manages the Issue Codes for their products.
The Admin can perform the below actions:
1. View the already existing Issue Codes
2. Add new issue codes with description
3. Change the status of the issue codes
4. Delete the newly added issue codes (Soft Delete). The ADMIN will not be able to delete the issue codes which are already saved in the DB (In Real Time scenario)
5. Validations has been provided such as Issue code must be of 17 characters, Issue Description can be of maximum 100 characters, no duplicate issue codes will be allowed, issue code and issue description is a mandatory field
6. Session Handling: 1 hour of inactivity will logout the user with a pop up confirming the same
7. If any unsaved data is there prompt will show up if the user tries to leave the particular screen
8. Sorting is enabled for the table columns
9. Once "Save" is clicked all the unsaved data will be saved to the DB (In real time scenario)
10.Toster alerts on 'Save'.
